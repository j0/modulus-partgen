use std::collections::VecDeque;
use std::fmt::format;
use std::path::PathBuf;
use std::vec;
use std::{fs::File, path::Path};
use std::io::prelude::*;

use glob::glob;
use image::{DynamicImage, GenericImageView, Rgba, RgbaImage, ImageBuffer};
use image::GenericImage;


#[derive(Clone, Debug)]
enum ToolTypes {
    Sword,
    Pickaxe,
    Hoe, 
    Shovel,
    Axe,
} 

#[derive(Clone, Debug)]
struct Tool {
    name: String,
    path: PathBuf,
    tool_type: ToolTypes,
}
    

fn main() -> Result<(), std::io::Error> {
    let swords = glob("./input/*_sword*").unwrap().into_iter().map(
        |t| {
            let t = t.unwrap(); Tool { name: t.file_stem().unwrap().to_string_lossy().to_string(), tool_type: ToolTypes::Sword, path: t} 
        }).collect::<Vec<Tool>>();

    let shovels = glob("./input/*_shovel*").unwrap().into_iter().map(
        |t| {
            let t = t.unwrap(); Tool { name: t.file_stem().unwrap().to_string_lossy().to_string(), tool_type: ToolTypes::Shovel, path: t} 
        }).collect::<Vec<Tool>>();

    let axes = glob("./input/*_axe*").unwrap().into_iter().map(
        |t| {
            let t = t.unwrap(); Tool { name: t.file_stem().unwrap().to_string_lossy().to_string(), tool_type: ToolTypes::Axe, path: t} 
        }).collect::<Vec<Tool>>();

    let pickaxes = glob("./input/*_pickaxe*").unwrap().into_iter().map(
        |t| {
            let t = t.unwrap(); Tool { name: t.file_stem().unwrap().to_string_lossy().to_string(), tool_type: ToolTypes::Pickaxe, path: t} 
        }).collect::<Vec<Tool>>();

    let hoes = glob("./input/*_hoe*").unwrap().into_iter().map(
        |t| {
            let t = t.unwrap(); Tool { name: t.file_stem().unwrap().to_string_lossy().to_string(), tool_type: ToolTypes::Hoe, path: t} 
        }).collect::<Vec<Tool>>();

    let tools = [swords, shovels, axes, pickaxes, hoes];

    generate_textures(tools.clone());
    generate_models(tools.clone());
    generate_tags(tools);

    Ok(())
}

fn generate_tags(tools: [Vec<Tool>; 5]) {

    let mut item_list = Vec::new();
    
    for list in tools {
        for entry in list {
            item_list.push(entry.name);
        }
    }

    generate_tag_json(item_list, "modular_tool_part".to_string());
}

fn generate_tag_json(item_list: Vec<String>, file_name: String) {


    
    let mut file = File::create(format!("./output/data/modulus/tags/items/{}.json", file_name)).unwrap();
    let mut buf = concat!(
        "{\n",
        "  \"replace\": \"false\"\n",
        "  \"values\": {\n"
    );

    let c = concat!(
        "  }\n",
        "}\n",
    );
    
    let mut string = String::from("");
    for item in item_list {
        string = format!("{string}    \"modulus:{}_module\"\n", item);
    }
    let combination = format!("{buf}{string}{c}");
    buf = combination.as_str();
    let buf = buf.as_bytes();
    
    file.write_all(buf).unwrap();

}


fn generate_models(tools: [Vec<Tool>; 5]) {

    for list in tools {
        for entry in list {

            generate_json(entry.clone(), String::from("a"), "unflipped");
            generate_json(entry.clone(), String::from("b"),  "unflipped");
            
            match entry.tool_type {
                ToolTypes::Axe | ToolTypes::Hoe => {
                    generate_json(entry.clone(), String::from("b"), "flipped");
                    generate_json(entry.clone(), String::from("b"), "flipped");
                },
                _ => {}
            }
            generate_json(entry, String::from("a"), "item");
        }
    }
}

fn generate_json(tool: Tool, side: String, mode: &str, ) {

    let file_name = match mode {
        "flipped" => format!("flipped_module_{}_{side}", tool.name),
        "unflipped" => format!("module_{}_{side}", tool.name),
        "item" => format!("{}_module", tool.name),
        _ => todo!(),
    };
    
    let mut file = File::create(format!("./output/assets/modulus/models/item/{}.json", file_name)).unwrap();
    let buf = concat!(
        "{\n",
        "   \"parent\": \"item/handheld\"\n",
        "   \"textures\": {\n"
    );

    let c = concat!(
        "   }\n",
        "}\n",
    );
    let string = format!("{buf}     \"layer0\": \"modulus:item/{}\"\n{}", file_name , c);
    let buf = string.as_str();
    let buf = buf.as_bytes();
    
    file.write_all(buf).unwrap();
}

fn apply_mask(base: RgbaImage, mask: DynamicImage) -> RgbaImage {

    let mut out: RgbaImage = ImageBuffer::new(16,16);

    for y in 1..base.height() {
        for x in 1..base.width() {
            let base_pixel = base.get_pixel(x, y);
            let mask_pixel = mask.get_pixel(x, y);

            if mask_pixel[3] == 0 {
                out.put_pixel(x, y, mask_pixel)
            } else {
                out.put_pixel(x, y, *base_pixel)
            }

        }
    }

    out

}

fn generate_textures(tools: [Vec<Tool>; 5]) {
    for list in tools {
        for entry in list {

            let entry_image = image::open(entry.path.clone()).unwrap();
            println!("{:?}", entry.path.file_stem());
            let tool_type = match entry.tool_type {
                ToolTypes::Sword => "sword",
                ToolTypes::Pickaxe => "pickaxe",
                ToolTypes::Hoe => "hoe",
                ToolTypes::Shovel => "shovel",
                ToolTypes::Axe => "axe",
            };

            // make tool part A
            apply_mask(
                    apply_mask(
                            entry_image.clone().into_rgba8(), 
                            image::open( format!("./rod_mask/{tool_type}.png")).unwrap()
                        ), 
                        image::open( format!("./b_mask/{tool_type}.png")).unwrap()
                    )
                .save(format!("./output/assets/modulus/textures/item/module_{}_a.png", entry.name)).unwrap();

            
            // make tool part B
            apply_mask(
                    apply_mask(
                            entry_image.clone().into_rgba8(), 
                            image::open( format!("./rod_mask/{tool_type}.png")).unwrap()
                        ), 
                        image::open( format!("./a_mask/{tool_type}.png")).unwrap()
                    )
                .save(format!("./output/assets/modulus/textures/item/module_{}_b.png", entry.name)).unwrap();

            match entry.tool_type {
                ToolTypes::Axe | ToolTypes::Hoe => {
                    let entry_image = entry_image.fliph().rotate90();

                    // make tool part A
                    apply_mask(
                            apply_mask(
                                    entry_image.clone().into_rgba8(), 
                                    image::open( format!("./rod_mask/{tool_type}.png")).unwrap().fliph().rotate90()
                                ), 
                                image::open( format!("./b_mask/{tool_type}.png")).unwrap().fliph().rotate90()
                            )
                        .save(format!("./output/assets/modulus/textures/item/flipped_module_{}_a.png", entry.name)).unwrap();

                    
                    // make tool part B
                    apply_mask(
                            apply_mask(
                                    entry_image.clone().into_rgba8(), 
                                    image::open( format!("./rod_mask/{tool_type}.png")).unwrap().fliph().rotate90()
                                ), 
                                image::open( format!("./a_mask/{tool_type}.png")).unwrap().fliph().rotate90()
                            )
                        .save(format!("./output/assets/modulus/textures/item/flipped_module_{}_b.png", entry.name)).unwrap();

                        },
                        _ => {},
                    }
            
            // make module item
            apply_mask(
                    entry_image.clone().into_rgba8(), 
                    image::open( format!("./rod_mask/{tool_type}.png")).unwrap()
            ).save(format!("./output/assets/modulus/textures/item/{}_module.png", entry.name)).unwrap();
        }
    }
    
}

// todo!("Palette swapping of wood.");